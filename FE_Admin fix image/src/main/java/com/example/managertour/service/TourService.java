package com.example.managertour.service;

import com.example.managertour.model.request.TourRequest;
import com.example.managertour.model.response.ApiResponse;
import com.example.managertour.model.response.TourResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface TourService {

    List<TourResponse> findAll();

    ApiResponse<TourResponse> findById(int id);

    ApiResponse<TourResponse> deleteById(int id);

    ApiResponse<TourResponse> saveTour(TourRequest tourRequest, MultipartFile file) throws IOException;

    //TourResponse: Thường được sử dụng khi chỉ cần trả về dữ liệu của một tour cụ thể hoặc danh sách các tour mà không cần xử lý lỗi hay các thông tin bổ sung khác. Ví dụ:
    //
    //TourResponse findById(int id): Trả về thông tin của tour có id chỉ định.
    //List<TourResponse> findAll(): Trả về danh sách tất cả các tour.
    //Trong các trường hợp này, TourResponse thường chứa các thông tin cụ thể về tour như id, name, description, price,...


    //ApiResponse<TourResponse>: Thường được sử dụng khi cần xử lý các trường hợp lỗi, gửi dữ liệu bằng phương thức POST, PUT hoặc DELETE
    // và cần có các thông tin bổ sung như mã lỗi, thông báo lỗi, trạng thái thành công hay thất bại của yêu cầu.

    //phương thức POST, PUT hoặc DELETE thường dùng api còn get thì không . get: lấy dl thôi

}
