package com.example.managertour.service.impl;

import com.example.managertour.model.request.TourRequest;
import com.example.managertour.model.response.ApiResponse;
import com.example.managertour.model.response.StatusEnum;
import com.example.managertour.model.response.TourResponse;
import com.example.managertour.service.TourService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.List;

@Service
public class TourServiceImpl implements TourService {

    @Autowired
    private RestTemplate restTemplate;


    private String apiUrl = "http://localhost:8888/api/admin/tours";

    @Override
    public List<TourResponse> findAll() {

        try {
            //xây dựng một URL để gửi yêu cầu HTTP đến một API bên ngoài dựa vào UriComponentsBuilder
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiUrl);

            // call Api
            ResponseEntity<ApiResponse<List<TourResponse>>> responseEntity = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    }
            );

            //lay dl tu phan hoi api
            ApiResponse<List<TourResponse>> apiResponse = responseEntity.getBody();

            if (apiResponse != null && apiResponse.getStatus().equals(StatusEnum.SUCCESS)) {
                return apiResponse.getPayload();
            }

            return null;

        } catch (Exception exception) {
            return null;
        }

    }

    @Override
    public ApiResponse<TourResponse> findById(int id) {
        try {

//            System.out.println("Fetching tour with ID: " + id);
//
//            Map<String, String> map = new HashMap<>();
//            map.put("id", String.valueOf(id));
//
//            //xây dựng một URL để gửi yêu cầu HTTP
//            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiUrl);
//
//            // gan tham so
//            for (Map.Entry<String, String> entry : map.entrySet()) {
//                builder.queryParam(entry.getKey(), entry.getValue());
//            }

            //call api:
            ResponseEntity<ApiResponse<TourResponse>> responseEntity = restTemplate.exchange(
                    apiUrl + "/" + id,
                    HttpMethod.GET,
                    null, //HttpEntity<?>
                    new ParameterizedTypeReference<>() {
                    }
            );

            //lay dl tu phan hoi api
            ApiResponse<TourResponse> apiResponse = responseEntity.getBody();
            if (apiResponse != null && apiResponse.getStatus().equals(StatusEnum.SUCCESS)) {
                return apiResponse;
            }
            return null;
        } catch (Exception exception) {
            return null;
        }
    }




//    @Override
//    public ApiResponse<TourResponse> saveTour(TourRequest tourRequest, MultipartFile file) {
//        try {
//            // Tạo đối tượng HttpHeaders và HttpEntity
//            HttpHeaders headers = new HttpHeaders();
//            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
//
//            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
//            body.add("tourName", tourRequest.getTourName());
//            body.add("itinerary", tourRequest.getItinerary());
//            body.add("startDate", tourRequest.getStartDate());
//            body.add("endDate", tourRequest.getEndDate());
//            body.add("price", tourRequest.getPrice());
//            body.add("maxSeats", tourRequest.getMaxSeats());
//            body.add("status", tourRequest.isStatus());
//
////            if (file != null && !file.isEmpty()) {
////                body.add("imgUrl", new ByteArrayResource(file.getBytes()));
////            }
//
//            if (file != null && !file.isEmpty()) {
//                Resource fileAsResource = new ByteArrayResource(file.getBytes()) {
//                    @Override
//                    public String getFilename() {
//                        return file.getOriginalFilename();
//                    }
//                };
//                body.add("imgUrl", fileAsResource);
//            }
//
//
//            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
//
//            // Gọi API và nhận response
//            ResponseEntity<ApiResponse<TourResponse>> responseEntity = restTemplate.exchange(
//                    apiUrl,
//                    HttpMethod.POST,
//                    requestEntity,
//                    new ParameterizedTypeReference<>() {} // Type reference for ApiResponse<TourResponse>
//            );
//
//            return responseEntity.getBody();
//
//        } catch (HttpClientErrorException exception) {
//            //Xử lý ngoại lệ HttpClientErrorException:
//            try {
//                if (exception.getStatusCode() != HttpStatus.OK) {
//
//                    ObjectMapper objectMapper = new ObjectMapper();
//
//                    ApiResponse apiResponse = objectMapper.readValue(exception.getResponseBodyAsString(), ApiResponse.class);
//
//                    return apiResponse;
//                }
//
//            } catch (JsonMappingException e) {
//                throw new RuntimeException(e);
//            } catch (JsonProcessingException e) {
//                throw new RuntimeException(e);
//            }
//
//        } catch (IOException exception) {
//
//            throw new RuntimeException();
//        }
//        return null;
//    }


    @Override
    public ApiResponse<TourResponse> saveTour(TourRequest tourRequest, MultipartFile file) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("tourName", tourRequest.getTourName());
            body.add("itinerary", tourRequest.getItinerary());
            body.add("startDate", tourRequest.getStartDate());
            body.add("endDate", tourRequest.getEndDate());
            body.add("price", tourRequest.getPrice());
            body.add("maxSeats", tourRequest.getMaxSeats());
            body.add("status", tourRequest.isStatus());

            // Add file as resource to formData if present
            if (!file.isEmpty()) {
                Resource fileAsResource = new ByteArrayResource(file.getBytes()) {
                    @Override
                    public String getFilename() {
                        return file.getOriginalFilename();
                    }
                };
                body.add("imgUrl", fileAsResource);
            }

            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

            // Call API and get response
            ResponseEntity<ApiResponse<TourResponse>> responseEntity = restTemplate.exchange(
                    apiUrl,
                    HttpMethod.POST,
                    requestEntity,
                    new ParameterizedTypeReference<>() {} // Type reference for ApiResponse<TourResponse>
            );

            return responseEntity.getBody();

        } catch (HttpClientErrorException exception) {
            // Handle HttpClientErrorException:
            try {
                if (exception.getStatusCode() != HttpStatus.OK) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    ApiResponse apiResponse = objectMapper.readValue(exception.getResponseBodyAsString(), ApiResponse.class);
                    return apiResponse;
                }
            } catch (JsonMappingException e) {
                throw new RuntimeException(e);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        return null;
    }




    @Override
    public ApiResponse<TourResponse> deleteById(int id) {

        try {
            //1. tạo httpHeader - httpEntity
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity httpEntity = new HttpEntity<>(id, httpHeaders);


            //2. call api

            ResponseEntity<ApiResponse<TourResponse>> responseEntity = restTemplate.exchange(
                    apiUrl + "/" + id,
                    HttpMethod.DELETE,
                    httpEntity,
                    new ParameterizedTypeReference<>() {
                    }
            );


            //3. lấy dl từ resp
            ApiResponse<TourResponse> apiResponse = responseEntity.getBody();
            return apiResponse;

        } catch (HttpClientErrorException ex) {

            try {
                if (ex.getStatusCode() != HttpStatus.OK){
                    ObjectMapper objectMapper = new ObjectMapper();
                    ApiResponse apiResponse = objectMapper.readValue(ex.getResponseBodyAsString(), ApiResponse.class);
                    return apiResponse;
                }
            } catch (Exception e) {
                return null;
            }
        }

        return null;
    }


}
