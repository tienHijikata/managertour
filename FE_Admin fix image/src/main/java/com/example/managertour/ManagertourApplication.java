package com.example.managertour;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManagertourApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManagertourApplication.class, args);
	}

}
