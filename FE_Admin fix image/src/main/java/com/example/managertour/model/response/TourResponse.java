package com.example.managertour.model.response;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

@Setter
@Getter
public class TourResponse implements Serializable {

    private Integer id;

    private String tourName;

    private String itinerary;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    private double price;

    private int maxSeats;

    private String imgUrl;

    private boolean status;

}
