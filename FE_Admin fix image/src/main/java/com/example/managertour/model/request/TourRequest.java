package com.example.managertour.model.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
public class TourRequest implements Serializable {

    private Integer id;

    @NotEmpty(message = "Name is not required!")
    private String tourName;

    @NotEmpty(message = "Itinerary is not required")
    private String itinerary;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @Positive(message = "Price is required!")
    private double price;

    @PositiveOrZero(message = "Max Seats is required!")
    private int maxSeats;

    private String imgUrlDatail;

    private MultipartFile imgUrl;

    private boolean status;
}
