package com.example.managertour.controller;

import com.example.managertour.model.response.TourResponse;
import com.example.managertour.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/admin")
public class TourListController {

    @Autowired
    private TourService tourService;

    @GetMapping(value = "/tour/list")
    public String tourList(Model model){

        List<TourResponse> tourList = tourService.findAll();

        for (TourResponse  tour : tourList){
            System.out.println(tour.getImgUrl());
        }

        model.addAttribute("tourList", tourList);

        return "admin/tour/list";
    }



}
