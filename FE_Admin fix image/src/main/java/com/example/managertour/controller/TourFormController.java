package com.example.managertour.controller;

import com.example.managertour.model.request.TourRequest;
import com.example.managertour.model.response.ApiResponse;
import com.example.managertour.model.response.StatusEnum;
import com.example.managertour.model.response.TourResponse;
import com.example.managertour.service.TourService;
import com.example.managertour.util.ValidatorUtil;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/admin")

public class TourFormController {

    @Autowired
    private TourService tourService;

    @Autowired
    private ValidatorUtil validatorUtil;



    @GetMapping(value = "/tour/new")
    public String tourNew(Model model) {

        TourResponse tourResponse = new TourResponse();
        tourResponse.setId(0);

        model.addAttribute("tour", tourResponse);
        model.addAttribute("errorMap", new LinkedHashMap<>());

        return "admin/tour/form";
    }


    @GetMapping("/tour/form/id")
    public String tourDetailByRequsetParam(Model model,
                             @RequestParam(value = "id", required = false) int id) {

        ApiResponse<TourResponse> apiResponse = tourService.findById(id);
        if (apiResponse == null || apiResponse.getStatus() != StatusEnum.SUCCESS){
            return "redirect:/admin/tour/list";
        }
        System.out.println("contr" + tourService.findById(id));
        model.addAttribute("tour", apiResponse);
        model.addAttribute("errorMap", new LinkedHashMap<>());

        return "admin/tour/form";
    }


    @GetMapping(value = "/tour/form/{id}")
    public String tourDetailByPath(Model model,
                             @PathVariable(value = "id", required = false) int id) {

        ApiResponse<TourResponse> apiResponse = tourService.findById(id);
        if (apiResponse == null || apiResponse.getStatus() != StatusEnum.SUCCESS){
            return "redirect:/admin/tour/list";
        }

        model.addAttribute("tour", apiResponse.getPayload());
        model.addAttribute("errorMap", new LinkedHashMap<>());

//        System.out.println(apiResponse.getPayload().toString());

        return "admin/tour/form";
    }




    @PostMapping(value = "/tour/form")
    public String tourSubmit(Model model,
                             @ModelAttribute(value = "tour") @Valid TourRequest tourRequest,
                             @RequestParam ("file") MultipartFile file,
                             BindingResult bindingResult) throws IOException {


        if (bindingResult.hasErrors()) {
            Map<String, String> errorMap = validatorUtil.toErrors(bindingResult.getFieldErrors());

            model.addAttribute("tour", tourRequest);
            model.addAttribute("errorMap", errorMap);
            return "admin/tour/form";

        }

        if (file.isEmpty()  || tourRequest.getImgUrl() == null) {

            Map<String, String> errorMap = validatorUtil.toErrors(bindingResult.getFieldErrors());
            model.addAttribute("tour", tourRequest);
            model.addAttribute("errorMap", errorMap);
            return "admin/tour/form";
        }

        ApiResponse<TourResponse> apiResponse = tourService.saveTour(tourRequest, file);

        if (apiResponse == null || apiResponse.getStatus() != StatusEnum.SUCCESS){

            Map<String, String> errorMap = apiResponse.getError();

            model.addAttribute("tour", tourRequest);
            model.addAttribute("errorMap", errorMap);
            return "admin/tour/form";
        }

        return "redirect:/admin/tour/list";
    }




    @DeleteMapping(value = "/tour/delete")
    @ResponseBody
    public ResponseEntity<String> tourDeleteById(
            @RequestParam (value = "id", required = false) int id) {

        ApiResponse<TourResponse> apiResponse = tourService.deleteById(id);
        if (apiResponse == null || apiResponse.getStatus() != StatusEnum.SUCCESS){
            return new ResponseEntity<>("NOT_FOUND", HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }



}
