package com.example.managertour.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class ViewController {
    @GetMapping("/")
    public String demo() {
        return "Admin/Dashboard/dashboard";
    }

    @GetMapping("/account/list")
    public String accountList() {
        return "Admin/Account/list";
    }


    @GetMapping("/login")
    public String login() {
        return "Admin/Login/register";
    }
}
