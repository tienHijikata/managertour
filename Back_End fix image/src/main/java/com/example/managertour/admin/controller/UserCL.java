//package com.example.managertour.admin.controller;
//
//@Slf4j
//@Service
//
//public class ProductServiceImpl implements ProductService {
//
//    @Autowired
//    CloudStorageService cloudStorageService;
//
//    @Autowired
//    AdminRepository adminRepository;
//
//    @Autowired
//    CategoryRepository categoryRepository;
//
//    @Autowired
//    PasswordEncoder passwordEncoder;
//
//    @Autowired
//    ModelMapper modelMapper;
//
//    @Autowired
//    UserPrincipalService userPrincipalService;
//
//    @Autowired
//    UserRepository userRepository;
//
//    @Autowired
//    ProductRepository productRepository;
//
//    @Override
//    public UploadProductResponseDto uploadProducts(
//            UploadProductRequestDto uploadProductRequestDto, StringcategoryName) throws AuthorizationException, GeneralServiceException, ImageUploadException {
//
//        Optional<Category> checkCategory = categoryRepository.findByCategoryName(categoryName);
//        if (checkCategory.isEmpty()){
//            throw new AuthorizationException(CATEGORY_NOT_RECOGNIZED);
//        }
//
//        Product product = new Product();
//        product=mapAdminRequestDtoToProduct(uploadProductRequestDto,product);
//        productRepository.save(product);
//        UploadProductResponseDto adminUploadProductResponseDto =
//                packageAdminProductUploadResponseDTO(product);
//        return adminUploadProductResponseDto;
//    }
//
//    private UploadProductResponseDto packageAdminProductUploadResponseDTO(Product product){
//        UploadProductResponseDto uploadProductResponseDto=new UploadProductResponseDto();
//        modelMapper.map(product,uploadProductResponseDto);
//        return uploadProductResponseDto;
//
//    }
//
//    private Product mapAdminRequestDtoToProduct(UploadProductRequestDto uploadProductRequestDto,Product product) throws ImageUploadException {
//
//        modelMapper.map(uploadProductRequestDto,product);
//        product=uploadProductImagesToCloudinaryAndSaveUrl(uploadProductRequestDto,product);
//        product.setId("Product "+ IdGenerator.generateId());
//        return product;
//
//    }
//
//
//    private Product uploadProductImagesToCloudinaryAndSaveUrl(UploadProductRequestDto uploadProductRequestDto,Product product) throws ImageUploadException {
//        product.setProductImage1(imageUrlFromCloudinary(uploadProductRequestDto.getProductImage1()));
//        product.setProductImage2(imageUrlFromCloudinary(uploadProductRequestDto.getProductImage2()));
//        product.setProductImage3(imageUrlFromCloudinary(uploadProductRequestDto.getProductImage3()));
//
//        return product;
//    }
//
//
//    private String imageUrlFromCloudinary(MultipartFile image) throws ImageUploadException {
//
//        String imageUrl="";
//
//        if(image!=null && !image.isEmpty()){
//            Map<Object,Object> params=new HashMap<>();
//            params.put("public_id","E&L/"+extractFileName(image.getName()));
//            params.put("overwrite",true);
//            try{
//                Map<?,?> uploadResult = cloudStorageService.uploadImage(image,params);
//                imageUrl= String.valueOf(uploadResult.get("url"));
//            }catch (IOException e){
//                e.printStackTrace();
//                throw new ImageUploadException("Error uploading images,vehicle upload failed");
//            }
//        }
//        return imageUrl;
//    }
//
//    private String extractFileName(String fileName){
//        return fileName.split("\\.")[0];
//    }
//}