//package com.example.managertour.admin.service.impl;
//
//import com.example.managertour.admin.service.AccountService;
//import com.example.managertour.admin.service.AuthService;
//import com.example.managertour.model.entity.Account;
//import com.example.managertour.model.request.AuthRequest;
//import com.example.managertour.model.request.LoginRequest;
//import com.example.managertour.model.request.RegisterRequest;
//import com.example.managertour.model.response.AuthResponse;
//import com.example.managertour.security.CustomUserDetails;
//import com.example.managertour.security.JwtToken;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Service;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Service
//public class AuthServiceImpl implements AuthService {
//    @Autowired
//    private AccountService userService;
//
//    @Autowired
//    private PasswordEncoder passwordEncoder;
//
//    @Autowired
//    private JwtToken jwtToken;
//
//    @Autowired
//    private AuthenticationManager authenticationManager;
//
//    @Override
//    public AuthResponse register(RegisterRequest request) {
//        Account user = new Account();
//        user.setName(request.getName());
//        user.setAddress(request.getAddress());
//        user.setEmail(request.getEmail());
//        user.setPhone(request.getPhone());
//        user.setPassword(passwordEncoder.encode(request.getPassword()));
//        user.setRole("USER");
//        user.setStatus(true);
//
//        userService.saveOrUpdate(user);
//
//        AuthResponse authResponse = new AuthResponse();
//        authResponse.setEmail(request.getEmail());
//
//        return authResponse;
//    }
//
//    @Override
//    public AuthResponse login(LoginRequest request) {
//        authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(
//                        request.getEmail(),
//                        request.getPassword()
//                )
//        );
//
//        Account user = userService.findByEmail(request.getEmail());
//        if (user != null) {
//            CustomUserDetails userSecurity = new CustomUserDetails(user);
//
//            Map<String, Object> extraClaims = new HashMap<>();
//            extraClaims.put("email", user.getEmail());
//            extraClaims.put("authorities", userSecurity.getAuthorities());
//
//            String accessToken = jwtToken.generateToken(extraClaims, userSecurity);
//            String refreshToken = jwtToken.generateRefreshToken(userSecurity);
//
//            AuthResponse authResponse = new AuthResponse();
//            authResponse.setEmail(request.getEmail());
//            authResponse.setAccessToken(accessToken);
//            authResponse.setRefreshToken(refreshToken);
//
//            return authResponse;
//        }
//
//        return null;
//    }
//
//    @Override
//    public AuthResponse refreshToken(AuthRequest request) {
//        return null;
//    }
//
//}
