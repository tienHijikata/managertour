package com.example.managertour.admin.service.impl;

import com.cloudinary.Cloudinary;
import com.example.managertour.admin.repository.TourRepository;
import com.example.managertour.admin.service.TourService;
import com.example.managertour.admin.util.ValidatorUtil;
import com.example.managertour.admin.validator.TourValidator;
import com.example.managertour.exception.ApplicationException;
import com.example.managertour.exception.ValidationException;
import com.example.managertour.model.entity.Tour;
import com.example.managertour.model.mapper.TourMapper;
import com.example.managertour.model.request.TourRequest;
import com.example.managertour.model.response.TourResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class TourServiceImpl implements TourService {

    @Autowired
    private TourRepository tourRepository;

    @Autowired
    private TourMapper tourMapper;

    @Autowired
    private TourValidator tourValidator;

    @Autowired
    private ValidatorUtil validatorUtil;

    @Autowired
    private Cloudinary cloudinary;


    @Override
    public Tour findById(int id) {
        try {

           return tourRepository.findById(id).orElse(null);

        }catch (ApplicationException exception){
            throw exception;
        }
    }


    @Override
    public List<Tour> findAll() {
       try {
           return tourRepository.findAll();
       }catch (ApplicationException exception){
           throw exception;
       }
    }



    @Override
    public TourResponse save(TourRequest tourRequest, BindingResult bindingResult) {
        try {
            //Validator
            tourValidator.validate(tourRequest, bindingResult);

            if (bindingResult.hasErrors()) {
                Map<String, String> validationErrors = validatorUtil.toError(bindingResult.getFieldErrors());
                throw new ValidationException(validationErrors);
            }


            // up ảnh lên Cloudinary
            String imageUrl = null;
            if (tourRequest.getImgUrl() != null && !tourRequest.getImgUrl().isEmpty()) {

                imageUrl = cloudinary.uploader()
                        .upload(tourRequest.getImgUrl().getBytes(),
                                Map.of("public_id", UUID.randomUUID().toString()))
                        .get("url").toString();
            }


            //Map to Entity
            Tour tour = tourMapper.toEntity(tourRequest);
            tour.setImgUrl(imageUrl);

            //Save
            tourRepository.saveAndFlush(tour);

            //Map to Response
            return tourMapper.toResponse(tour);

        } catch (ApplicationException exception) {
            throw exception;
        } catch (Exception exception) {
            throw new ApplicationException();
        }
    }

    @Override
    public TourResponse update(TourRequest tourRequest, BindingResult bindingResult) {

        try {
            // Kiem tra Tour Exist ?
            Tour existingTour = findById(tourRequest.getId());
            if( existingTour == null){
                throw new ApplicationException("Tour Not Found");
            }

            //Validator
            tourValidator.validate(tourRequest,bindingResult);
            if (bindingResult.hasErrors()) {
                Map<String, String> errors = validatorUtil.toError(bindingResult.getFieldErrors());
                throw new ValidationException(errors);
            }

            //To Entity
            Tour tour = tourMapper.toEntity(tourRequest);

            //Update
            tourRepository.saveAndFlush(tour);

            //To Response
            return tourMapper.toResponse(tour);

        }catch (ApplicationException exception){
            throw exception;
        }
    }

    @Override
    public void delete(int id) {
    try {

        // kiem tra Tour Exist ?
        Tour tour = tourRepository.findById(id).orElse(null);
        if(tour == null){
            throw new ApplicationException("Tour Not Found");
        }

        //SetStatus
        tour.setStatus(false);
        tourRepository.saveAndFlush(tour);

    }catch (ApplicationException exception){
        throw exception;
    }

    }

}
