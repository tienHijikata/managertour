package com.example.managertour.admin.service;

import com.example.managertour.model.entity.Tour;
import com.example.managertour.model.request.TourRequest;
import com.example.managertour.model.response.TourResponse;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface TourService {

    Tour findById(int id);

    List<Tour> findAll();

    TourResponse save(TourRequest tourRequest, BindingResult bindingResult);

    TourResponse update(TourRequest tourRequest, BindingResult bindingResult);

    void delete(int id);


}
