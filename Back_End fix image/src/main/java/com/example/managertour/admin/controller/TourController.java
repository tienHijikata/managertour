package com.example.managertour.admin.controller;


import com.example.managertour.admin.exception.ValidationException;
import com.example.managertour.admin.service.TourService;
import com.example.managertour.admin.util.ValidatorUtil;
import com.example.managertour.exception.ApplicationException;
import com.example.managertour.exception.NotFoundException;
import com.example.managertour.model.entity.Tour;
import com.example.managertour.model.mapper.TourMapper;
import com.example.managertour.model.request.TourRequest;
import com.example.managertour.model.response.ApiResponse;
import com.example.managertour.model.response.TourResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/admin/tours")
public class TourController {

    //localhost:8888/api/admin/tours
    @Autowired
    private TourService tourService;

    @Autowired
    private TourMapper tourMapper;

    @Autowired
    private ValidatorUtil validatorUtil;


    @GetMapping({"", "/list"})
    public ResponseEntity<ApiResponse<List<TourResponse>>> getAllTour() {
        try {
            //Get data from Service
            List<Tour> tourList = tourService.findAll();

            // To Response ->  ApiRespone
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.ok(tourMapper.toResponseList(tourList));

            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        } catch (Exception e) {
            throw new ApplicationException();
        }
    }

    @GetMapping("{tourId}")
    public ResponseEntity<ApiResponse<TourResponse>> getById(@PathVariable int tourId) {
        try {
            Tour tour = tourService.findById(tourId);
            if (tour == null) {
                throw new ApplicationException();
            }

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.ok(tourMapper.toResponse(tour));
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);

        } catch (NotFoundException exception) {
            throw exception;
        } catch (Exception exception) {
            throw exception;
        }
    }

    @GetMapping({"/tourId"})
    public ResponseEntity<ApiResponse<TourResponse>> getByIdByRestParam(@RequestParam int tourId) {

        try {
            Tour tour = tourService.findById(tourId);
            if (tour == null) {
                throw new ApplicationException();
            }

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.ok(tourMapper.toResponse(tour));
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);

        } catch (NotFoundException exception) {
            throw exception;
        } catch (Exception exception) {
            throw exception;
        }
    }



    @PostMapping("")
    public ResponseEntity<ApiResponse> save(@Valid @ModelAttribute TourRequest tourRequest,
                                            BindingResult bindingResult) {
        try {

            if (bindingResult.hasErrors()) {
                Map<String, String> validationErrors = validatorUtil.toError(bindingResult.getFieldErrors());
                throw new ValidationException(validationErrors);
            }


            TourResponse tourResponse = tourService.save(tourRequest, bindingResult);


            ApiResponse apiResponse = new ApiResponse();
            apiResponse.ok(tourResponse);

            return ResponseEntity.ok(apiResponse);

        } catch (ValidationException exception) {
            throw exception;
        } catch (Exception exception) {
            throw exception;
        }
    }



    @PutMapping("")
    public ResponseEntity<ApiResponse> update(@Valid @RequestBody
                                              TourRequest tourRequest, BindingResult bindingResult) {
        try {
            TourResponse tourResponse = tourService.update(tourRequest, bindingResult);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.ok(tourResponse);

            return ResponseEntity.ok(apiResponse);

        } catch (ValidationException exception) {
            throw exception;
        } catch (Exception exception) {
            throw new ApplicationException();
        }
    }


    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse<TourResponse>> updateTourByPathValiable(
            @PathVariable("id") int tourId,
            @Valid @RequestBody TourRequest tourRequest,
            BindingResult bindingResult) {

        try {
//             Validate input
//            tourValidator.validate(tourRequest, bindingResult);
//            if (bindingResult.hasErrors()) {
//                Map<String, String> errors = validatorUtil.toError(bindingResult.getFieldErrors());
//                throw new ValidationException(errors);
//            }

            // Check if tour exists
            Tour existingTour = tourService.findById(tourId);
            if (existingTour == null) {
                throw new NotFoundException("Tour not found");
            }

            tourRequest.setId(tourId);

            TourResponse updatedTour = tourService.update(tourRequest, bindingResult);

            // Return response
            ApiResponse<TourResponse> apiResponse = new ApiResponse<>();
            apiResponse.ok(updatedTour);

            return ResponseEntity.ok(apiResponse);
        } catch (ValidationException ex) {
            Map<String, String> error = new HashMap<>();
            error.put("validation_error", ex.getMessage());

            ApiResponse<TourResponse> apiResponse = new ApiResponse<>();
            apiResponse.error(error);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiResponse);
        } catch (NotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new ApplicationException();
        }
    }


    @DeleteMapping("{tourId}")
    public ResponseEntity<ApiResponse> delete(@PathVariable int tourId) {
        try {
            tourService.delete(tourId);

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.ok();

            return ResponseEntity.ok(apiResponse);
        } catch (NotFoundException exception) {
            throw exception;
        } catch (Exception exception) {
            throw new ApplicationException();
        }
    }

}

