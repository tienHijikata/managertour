package com.example.managertour.admin.service;

import com.example.managertour.model.request.AuthRequest;
import com.example.managertour.model.request.LoginRequest;
import com.example.managertour.model.request.RegisterRequest;
import com.example.managertour.model.response.AuthResponse;

public interface AuthService {
    AuthResponse register(RegisterRequest request);

    AuthResponse login(LoginRequest request);

    AuthResponse refreshToken(AuthRequest request);

}
