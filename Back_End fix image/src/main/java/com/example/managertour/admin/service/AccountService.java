package com.example.managertour.admin.service;

import com.example.managertour.model.entity.Account;

public interface AccountService {
    Account findByEmail(String email);

    Account saveOrUpdate(Account account);

}
