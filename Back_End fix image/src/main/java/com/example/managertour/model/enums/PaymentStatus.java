package com.example.managertour.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum PaymentStatus {
    NOT_PAID("Not yet paid"),
    IN_PROGRESS("In Progress"),
    PAID("Paid");

    private final String enumValue;

    PaymentStatus(String enumValue) {
        this.enumValue = enumValue;
    }

    @JsonValue
    public String value() {
        return enumValue;
    }

    @JsonCreator
    public static PaymentStatus fromValue(String value) {
        for (PaymentStatus status : PaymentStatus.values()) {
            if (status.value().equals(value)) {
                return status;
            }
        }
        throw new IllegalArgumentException("Unknown code: " + value);
    }
}
