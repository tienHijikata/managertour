package com.example.managertour.model.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@Table(name = "bookings")
public class Booking implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "booking_id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @ManyToOne
    @JoinColumn(name = "tour_id", nullable = false)
    private Tour tour;

    @Column(name = "booking_date", updatable = false)
    private LocalDateTime bookingDate;

    @Min(value = 1, message = "Number of people must be at least 1")
    @Column(name = "number_of_people")
    private int numberOfPeople;

    private boolean status;

    @OneToOne(mappedBy = "booking", cascade = CascadeType.ALL)
    private Payment payment;

    @ManyToOne
    @JoinColumn(name = "promotion_id")
    private Promotion promotion;

    @PrePersist
    protected void onCreate() {
        bookingDate = LocalDateTime.now();
    }
}
