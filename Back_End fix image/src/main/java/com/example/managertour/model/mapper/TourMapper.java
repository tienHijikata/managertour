package com.example.managertour.model.mapper;


import com.example.managertour.model.entity.Tour;
import com.example.managertour.model.request.TourRequest;
import com.example.managertour.model.response.TourResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TourMapper {

    //Map Request to Entity
    @Mapping(target = "imgUrl", source = "imgUrl.originalFilename")
    Tour toEntity(TourRequest tourRequest);

    List<Tour> toEntityList(List<TourRequest> tourRequestList);

    //Map Entity to Response
    TourResponse toResponse(Tour tour);

    List<TourResponse> toResponseList(List<Tour> tourList);

}
