package com.example.managertour.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum CommentStatus {
    PENDING("Pending"),
    PUBLISHED("Published");

    private final String enumValue;

    CommentStatus(String enumValue) {
        this.enumValue = enumValue;
    }

    @JsonValue
    public String value() {
        return enumValue;
    }

    @JsonCreator
    public static CommentStatus fromValue(String value) {
        for (CommentStatus status : CommentStatus.values()) {
            if (status.value().equals(value)) {
                return status;
            }
        }
        throw new IllegalArgumentException("Unknown code: " + value);
    }
}
