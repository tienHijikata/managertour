package com.example.managertour.model.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "tours")
public class Tour implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tour_id")
    private Integer id;

//    @ManyToOne
//    @JoinColumn(name = "promotion_id")
//    private Promotion promotion;

    @NotNull(message = "Tour name cannot be null")
    @Column(name = "tour_name", unique = true)
    private String tourName;

    private String itinerary;

    @NotNull(message = "Start date cannot be null")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "start_date")
    private LocalDate startDate;

    @NotNull(message = "End date cannot be null")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "end_date")
    private LocalDate endDate;

    @Min(value = 0, message = "Price must be positive")
    private double price;

    @Column(name = "max_seats")
    private int maxSeats;

    @Column(name = "img_url")
    private String imgUrl;

    private boolean status;

    @OneToMany(mappedBy = "tour", cascade = CascadeType.ALL)
    private List<Booking> bookingList;

    @OneToMany(mappedBy = "tour", cascade = CascadeType.ALL)
    private List<Comment> commentList;
}
