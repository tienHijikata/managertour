package com.example.managertour.model.response;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

@Setter
@Getter
public class TourResponse implements Serializable {

    private Integer id;

    private String tourName;

    private String itinerary;

    private LocalDate startDate;

    private LocalDate endDate;

    private double price;

    private int maxSeats;

    private String imgUrl;

    private boolean status;

}
