package com.example.managertour.model.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "promotions")
public class Promotion implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "promotion_id")
    private Integer id;

    @NotNull(message = "Promotion code cannot be null")
    @Column(name = "promotion_code", unique = true)
    private String promotionCode;

    private String description;

    @Min(value = 0, message = "Discount percentage must be positive")
    @Max(value = 100, message = "Discount percentage cannot exceed 100")
    @Column(name = "discount_percentage")
    private double discountPercentage;

    private boolean status;

//    @OneToMany(mappedBy = "promotion", cascade = CascadeType.ALL, orphanRemoval = true)
//    private List<Tour> tourList;

    @OneToMany(mappedBy = "promotion", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Booking> bookings;

}
