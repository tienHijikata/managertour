package com.example.managertour.model.request;

import jakarta.persistence.Column;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.time.LocalDate;

@Setter
@Getter
public class TourRequest implements Serializable {


    private Integer id;

    @NotEmpty(message = "Name is not required!")
    private String tourName;

    @NotEmpty(message = "Itinerary is not required")
    private String itinerary;

    private LocalDate startDate;

    private LocalDate endDate;

    @Positive(message = "Price is required!")
    private double price;

    private MultipartFile imgUrl;

    @PositiveOrZero(message = "Max Seats is required!")
    private int maxSeats;

    private boolean status;


}
