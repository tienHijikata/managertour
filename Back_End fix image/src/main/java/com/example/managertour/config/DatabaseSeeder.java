package com.example.managertour.config;

import com.example.managertour.admin.repository.*;
import com.example.managertour.model.entity.*;
import com.example.managertour.model.enums.CommentStatus;
import com.example.managertour.model.enums.PaymentStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DatabaseSeeder implements CommandLineRunner {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TourRepository tourRepository;
    @Autowired
    private PromotionRepository promotionRepository;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private PaymentRepository paymentRepository;
    @Autowired
    private CommentRepository commentRepository;

    @Override
    public void run(String... args) throws Exception {
        seedAccounts();
        seedTours();
        seedPromotions();
        seedBookings();
        seedPayments();
        seedComments();
    }

    private void seedAccounts() {
        if (accountRepository.count() == 0) {
            Account account1 = new Account();
            account1.setStatus(true);
            account1.setAddress("123 Main St");
            account1.setEmail("user1@example.com");
            account1.setName("User One");
            account1.setPassword("password");
            account1.setPhone("1234567890");
            account1.setRole("User");
            accountRepository.save(account1);

            Account account2 = new Account();
            account2.setStatus(true);
            account2.setAddress("456 Second St");
            account2.setEmail("user2@example.com");
            account2.setName("User Two");
            account2.setPassword("password");
            account2.setPhone("0987654321");
            account2.setRole("Admin");
            accountRepository.save(account2);

            Account account3 = new Account();
            account3.setStatus(true);
            account3.setAddress("789 Third St");
            account3.setEmail("user3@example.com");
            account3.setName("User Three");
            account3.setPassword("password");
            account3.setPhone("1122334455");
            account3.setRole("User");
            accountRepository.save(account3);
        }
    }

    private void seedTours() {
        if (tourRepository.count() == 0) {
            Tour tour1 = new Tour();
            tour1.setEndDate(LocalDate.of(2024, 3,1));
            tour1.setMaxSeats(20);
            tour1.setPrice(1000);
            tour1.setStartDate(LocalDate.of(2024, 1,1));
            tour1.setStatus(true);
            tour1.setImgUrl("http://res.cloudinary.com/daivtsrxb/image/upload/v1719038143/6f3d40e7-4d02-49b6-b818-0a111f23b1c8.jpg");
            tour1.setItinerary("Day 1: Arrival\nDay 2: City Tour\n...");
            tour1.setTourName("Amazing Tour");
            tourRepository.save(tour1);

            Tour tour2 = new Tour();
            tour2.setEndDate(LocalDate.of(2024, 4,15));
            tour2.setMaxSeats(15);
            tour2.setPrice(800);
            tour2.setStartDate(LocalDate.of(2024, 4,10));
            tour2.setStatus(true);
            tour2.setImgUrl("http://res.cloudinary.com/daivtsrxb/image/upload/v1719038801/8eed6ce0-4c26-43cb-955f-b20ebc083517.jpg");
            tour2.setItinerary("Day 1: Beach\nDay 2: Mountain\n...");
            tour2.setTourName("Adventure Tour");
            tourRepository.save(tour2);

            Tour tour3 = new Tour();
            tour3.setEndDate(LocalDate.of(2024, 5,12));
            tour3.setMaxSeats(25);
            tour3.setPrice(1200);
            tour3.setStartDate(LocalDate.of(2024, 5,2));
            tour3.setStatus(true);
            tour3.setImgUrl("http://res.cloudinary.com/daivtsrxb/image/upload/v1719043282/c0b7c773-fa63-4c90-9d08-b292add19ea4.jpg");
            tour3.setItinerary("Day 1: Safari\nDay 2: Jungle\n...");
            tour3.setTourName("Wildlife Tour");
            tourRepository.save(tour3);
        }
    }

    private void seedPromotions() {
        if (promotionRepository.count() == 0) {
            Promotion promotion1 = new Promotion();
            promotion1.setDiscountPercentage(10);
            promotion1.setStatus(true);
            promotion1.setDescription("10% off on all tours");
            promotion1.setPromotionCode("PROMO10");
            promotionRepository.save(promotion1);

            Promotion promotion2 = new Promotion();
            promotion2.setDiscountPercentage(15);
            promotion2.setStatus(true);
            promotion2.setDescription("15% off on Adventure tours");
            promotion2.setPromotionCode("ADVENTURE15");
            promotionRepository.save(promotion2);

            Promotion promotion3 = new Promotion();
            promotion3.setDiscountPercentage(20);
            promotion3.setStatus(true);
            promotion3.setDescription("20% off on Wildlife tours");
            promotion3.setPromotionCode("WILDLIFE20");
            promotionRepository.save(promotion3);
        }
    }

    private void seedBookings() {
        if (bookingRepository.count() == 0) {
            Account account1 = accountRepository.findById(1).orElse(null);
            Account account2 = accountRepository.findById(2).orElse(null);
            Account account3 = accountRepository.findById(3).orElse(null);
            Promotion promotion1 = promotionRepository.findById(1).orElse(null);
            Promotion promotion2 = promotionRepository.findById(2).orElse(null);
            Tour tour1 = tourRepository.findById(1).orElse(null);
            Tour tour2 = tourRepository.findById(2).orElse(null);
            Tour tour3 = tourRepository.findById(3).orElse(null);

            if (account1 != null && promotion1 != null && tour1 != null) {
                Booking booking1 = new Booking();
                booking1.setNumberOfPeople(2);
                booking1.setStatus(true);
                booking1.setAccount(account1);
                booking1.setPromotion(promotion1);
                booking1.setTour(tour1);
                bookingRepository.save(booking1);
            }

            if (account2 != null && promotion2 != null && tour2 != null) {
                Booking booking2 = new Booking();
                booking2.setNumberOfPeople(3);
                booking2.setStatus(true);
                booking2.setAccount(account2);
                booking2.setPromotion(promotion2);
                booking2.setTour(tour2);
                bookingRepository.save(booking2);
            }

            if (account3 != null && promotion1 != null && tour3 != null) {
                Booking booking3 = new Booking();
                booking3.setNumberOfPeople(1);
                booking3.setStatus(true);
                booking3.setAccount(account3);
                booking3.setPromotion(promotion1);
                booking3.setTour(tour3);
                bookingRepository.save(booking3);
            }
        }
    }

    private void seedPayments() {
        if (paymentRepository.count() == 0) {
            Booking booking1 = bookingRepository.findById(1).orElse(null);
            Booking booking2 = bookingRepository.findById(2).orElse(null);
            Booking booking3 = bookingRepository.findById(3).orElse(null);

            if (booking1 != null) {
                Payment payment1 = new Payment();
                payment1.setAmount(900); // 10% discount applied
                payment1.setStatus(PaymentStatus.NOT_PAID);
                payment1.setBillUrl("http://example.com/bill1.pdf");
                payment1.setBooking(booking1);
                paymentRepository.save(payment1);
            }

            if (booking2 != null) {
                Payment payment2 = new Payment();
                payment2.setAmount(680); // 15% discount applied
                payment2.setStatus(PaymentStatus.NOT_PAID);
                payment2.setBillUrl("http://example.com/bill2.pdf");
                payment2.setBooking(booking2);
                paymentRepository.save(payment2);
            }

            if (booking3 != null) {
                Payment payment3 = new Payment();
                payment3.setAmount(960); // 20% discount applied
                payment3.setStatus(PaymentStatus.PAID);
                payment3.setBillUrl("http://example.com/bill3.pdf");
                payment3.setBooking(booking3);
                paymentRepository.save(payment3);
            }
        }
    }

    private void seedComments() {
        if (commentRepository.count() == 0) {
            Account account1 = accountRepository.findById(1).orElse(null);
            Account account2 = accountRepository.findById(2).orElse(null);
            Account account3 = accountRepository.findById(3).orElse(null);
            Tour tour1 = tourRepository.findById(1).orElse(null);
            Tour tour2 = tourRepository.findById(2).orElse(null);
            Tour tour3 = tourRepository.findById(3).orElse(null);

            if (account1 != null && tour1 != null) {
                Comment comment1 = new Comment();
                comment1.setRating(5);
                comment1.setStatus(CommentStatus.PENDING);
                comment1.setContent("Amazing tour! Highly recommend.");
                comment1.setAccount(account1);
                comment1.setTour(tour1);
                commentRepository.save(comment1);
            }

            if (account2 != null && tour2 != null) {
                Comment comment2 = new Comment();
                comment2.setRating(4);
                comment2.setStatus(CommentStatus.PUBLISHED);
                comment2.setContent("Great experience, will join again.");
                comment2.setAccount(account2);
                comment2.setTour(tour2);
                commentRepository.save(comment2);
            }

            if (account3 != null && tour3 != null) {
                Comment comment3 = new Comment();
                comment3.setRating(3);
                comment3.setStatus(CommentStatus.PUBLISHED);
                comment3.setContent("Good but could be better.");
                comment3.setAccount(account3);
                comment3.setTour(tour3);
                commentRepository.save(comment3);
            }
        }
    }
}
